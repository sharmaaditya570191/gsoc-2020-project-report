# :speech_balloon: USER STORY 

![](./assets/user_story.png)

## Project Summary

### Problem

Most users of `EOS Design System` and `EOS Icons` do not want to sign in to a developer friendly environment like GitLab to send an issue and want something faster, convenient and efficient within the same interface of EOS.

### Solution

The goal of `User Story` is to design and present a scalable backend infrastructure that delivers a web interface allowing users to request new features in an easy and intuitive way. Users can attach files with the request to explain what they want. The admins can then resolve, close and revise these requests. Users can comment and vote for the existing requests to move them to priority. This can also serve as an efficient feedback and response mechanism which is critical for any organization to improve and make progress. Thus, it potentially becomes another reusable open source project for EOS. Simply, its a **feedback cum feature request management system**.

## How it works

![](./assets/user_story_workflow.png)

## Main features

### Users

- Unlimited stories for users to share their feedback, experience or request new features.
- Vote, relate and interact with other users' stories so we know what our users want most
- Get notifications when your feature is ready and deployed
- Follow stories of your interest and get notified when they are launched. This eleminates creation of duplicate stories.
- Manage and check status of all stories you have created or followed in a click via user profiles
- Attach demo videos, images, pdf, charts to explain your needs clearly
- Get updates on privacy policy and options to accept or remove your account data or simply logout
- Check other user profiles and read their stories
- Sort stories by Votes or Most Discussed

### Admins

- Get access to 100% customizable Strapi admin interface connected directly to database
- Admins can manage various products and customize product roadmaps according to their organization workflow
- Change status of stories in a click and generate automatic notifications
- Update privacy policy on the fly
- Block users who violate code of conduct
- Delete stories which are irrelevant to the product
- Analyze and spend time on the most required feature or fix

## My Journey

I have written about my journey to GSoC, tips and tricks to get selected and what all you can do before GSoC to take a step closer to your dream career.

[From console.log to GSoC 2020](https://medium.com/@sharmaaditya570191/from-console-log-to-gsoc-2020-1a6e9dc2334e)

I have also written blogs every week to share my GSoC work, the problems I faced and how I overcame all obstacles.

[GSoC Technical Blogs](https://medium.com/@sharmaaditya570191) 

## My role and technologies involved

- Built a custom and secure feature request system with Koa.js, Strapi and React.js.
- Used a mix of embedded and referenced relationships optimized for our overall model structure in MongoDB.
- Secured user authentication system using httpOnly cookies and custom policies and controllers.
- Used GraphQL plugin in Strapi to reduce server response time from 1500ms to 200ms.
- Wrote end-to-end and unit tests for client side using Cypress, Mocha and Chai.

## My code contributions

- [Pull requests to client side repository](https://gitlab.com/SUSE-UIUX/eos-user-story/-/merge_requests?scope=all&utf8=%E2%9C%93&state=all&author_username=sharmaaditya570191)
- [Pull requests to server side repository](https://gitlab.com/SUSE-UIUX/eos-strapi/-/merge_requests?scope=all&utf8=%E2%9C%93&state=all&author_username=sharmaaditya570191)

## New ideas

I have some interesting ideas in mind. These can be implemented after Google Summer of Code.

- Integration of User Story with tools in our existing workflow like Slack and GitLab.
- Personalized email notifications.
- Increase test coverage by writing more unit tests for edge cases.
- Integrating end-to-end tests in CI to eliminate the need of manual testing for future PR's.
- Search stories via home page
- Allow users and admins to add priority labels to stories like `Important`, `Critical`, and `Moderate` while creating new stories.
- Generate shareable link with 1 click to enable easy sharing of stories among users across various platforms
- Quick chats and meetings for users and admins to plan and discuss ideas and stories.
- Enable admins to go live via the platform to interact with users and understand their needs.
- Migration to TypeScript to add static types
- Add some of the above features under a `Premium` category and give it to free to users who are active on our platform. This will eventually bring in more users.

## Thank You

My sweet GSOC family literally coded the summer away with some of the best developers and designers from around the world always ready to hold my hand in case I loose balance.  

I thank all my mentors for giving me the best experience of my life. The care, motivation and guidance I got from you all will definitely help me in the days to come.  

Time flew and brought us close to the end. However, the best things deserve to be a special part of our life and EOS had already made its place in my heart long back. Loads of 💖 to my EOS family.  

We will keep improving the user as well as developer experience in User Story in the coming days. Remember, Elon has already set his eyes on this. :stuck_out_tongue_winking_eye: 

Cheers to a new beginning. :v: